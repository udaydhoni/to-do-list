let inputField = document.querySelector('.input-bar');

inputField.placeholder = 'What needs to be done?'

let taskList = document.querySelector('.task-list');

let footer = document.querySelector('.footer-menu');

let allBtn = document.querySelector('.All');

let activeBtn = document.querySelector('.Active');



let completedBtn = document.querySelector('.Completed')


let tasks = document.querySelector('.list')

let clearBtn = document.querySelector('.clear')

let taskObject = {notCompleted:[]};


let statusOfTasks = document.querySelector('.rem-tasks');

let activeCount = 0;

let arrowBtn = document.querySelector('.arrow');

inputField.addEventListener('keydown', (event)=>{
   
    
    if (`${event.key}` == 'Enter' && inputField.value != '') {
        let newTask = document.createElement('li');
        let para = document.createElement('p');
        newTask.style.width = '900px';
        newTask.style.height = '60px';
        newTask.style.listStyle= 'none';
        newTask.style.border = '1px solid black';
        para.innerText = inputField.value;
        para.style.overflowWrap = 'word-break';
        para.classList.add('activity');
        newTask.style.display = 'flex';
        newTask.style.justifyContent = 'space-between';
        newTask.style.backgroundColor = 'white';
        newTask.append(para);




        // delete button
        let cross = document.createElement('button');
        cross.id = 'delete'
        cross.innerText = 'delete';
        cross.style.background = 'white';

        
        newTask.append(cross);
        

        //completed button
        let donebtn = document.createElement('button');
        donebtn.innerText = 'done';
        donebtn.id = 'done'
        donebtn.style.borderRadius = '100%';
        donebtn.style.border= '2px solid black';
        donebtn.style.height = '45px';
        donebtn.style.marginLeft = '10px';
        donebtn.style.marginTop = '8px';
        donebtn.style.backgroundColor = 'white';
        donebtn.classList.add('not-active');
        

        newTask.prepend(donebtn);

        
            tasks.append(newTask);
            footer.style.display = 'block';
            footer.style.display = 'flex';
            footer.style.justifyContent ='space-around';
            footer.style.width = '900px';
            taskObject['notCompleted'].push(newTask);
            activeCount+=1;
            statusOfTasks.innerText = `${activeCount} tasks are remaining`
            console.log(taskObject);
        inputField.value = null;

    }
    
})

taskList.addEventListener('click',(event)=>{
    

    if (event.target.innerText == 'delete') {
        activeCount =0;
        let taskItem = event.target.parentElement;
        taskItem.remove();
        let index = 0;
        for (let i =0; i<taskObject['notCompleted'].length; i++) {
            if (taskObject['notCompleted'][i].innerText == taskItem.innerText) {
                index = i;
                break;
            }
        }
        taskObject['notCompleted'].splice(index,1)
        taskObject['notCompleted'].forEach((elem)=>{
            if (!(elem.querySelector('.activity').classList.contains('strike-through'))) {
                activeCount +=1;
            }
        })
        statusOfTasks.innerText = `${activeCount} tasks are remaining`
    }

    if (event.target.innerText == 'done') {
        event.target.classList.toggle('change-color');
        console.log(event.target.classList);
        activeCount = 0;
        let taskItem = event.target.parentElement;
        let taskText = taskItem.querySelector('.activity');
        taskText.classList.toggle('strike-through');         // There is a bug here.... 
        clearBtn.style.display = 'none';
        Array.from(tasks.children).forEach((elem)=>{
            console.log(elem.querySelector('.activity'));
            if (elem.querySelector('.activity').classList.contains('strike-through')) {
                clearBtn.style.display = 'inline-block';
                
            }
            if (!(elem.querySelector('.activity').classList.contains('strike-through'))) {
                activeCount+=1
            }
        })
        statusOfTasks.innerText = `${activeCount} tasks are remaining`
    }

}
)

activeBtn.addEventListener ('click', ()=>{
    console.log(tasks.children);
    Array.from(tasks.children).forEach((elem)=>{
        elem.remove();
    })
    taskObject['notCompleted'].forEach((elem)=>{
        console.log(elem);
        let taskPara = elem.querySelector('p');
        console.log(taskPara.classList.contains('strike-through'));
        if (!(taskPara.classList.contains('strike-through'))) {
            tasks.append(elem);
        }
    })
})

completedBtn.addEventListener('click',()=>{
    Array.from(tasks.children).forEach((elem)=>{
        elem.remove();
    })
    taskObject['notCompleted'].forEach((elem)=>{
        let taskPara = elem.querySelector('p');
        if (!(taskPara.classList.contains('strike-through'))) {
            elem.remove();
        } else {
            tasks.append(elem)
        }
    })
})

allBtn.addEventListener('click',()=>{
    Array.from(tasks.children).forEach((elem)=>{
        elem.remove();
    })
    taskObject['notCompleted'].forEach((elem)=>{
        tasks.append(elem);
    })
})

clearBtn.addEventListener('click',()=>{
    Array.from(tasks.children).forEach((elem)=>{
        elem.remove();
    })
    let newTaskObject = {};
     newTaskObject['notCompleted'] = [];

    for (let i =0; i<taskObject['notCompleted'].length; i++) {
        console.log(taskObject['notCompleted'][i].querySelector('.activity'));
        if (!(taskObject['notCompleted'][i].querySelector('.activity').classList.contains('strike-through'))) {
            newTaskObject['notCompleted'].push(taskObject['notCompleted'][i])
        }
    }
    taskObject['notCompleted'] = newTaskObject['notCompleted'];
    taskObject['notCompleted'].forEach((elem)=>{
        tasks.append(elem);
    })
    clearBtn.style.display = 'none';
})


arrowBtn.addEventListener('click', ()=> {
    activeCount = 0;

    let toggled = 0;

    taskObject['notCompleted'].forEach((ele)=>{
        if (ele.querySelector('.activity').classList.contains('strike-through')) {
            toggled +=1;
        }
    })

    console.log(toggled);
    if (toggled == 0 | toggled == taskObject['notCompleted'].length) {
        taskObject['notCompleted'].forEach((ele)=>{
                ele.querySelector('.activity').classList.toggle('strike-through');
            
        })
    } else {
        taskObject['notCompleted'].forEach((ele)=>{
            if (!(ele.querySelector('.activity').classList.contains('strike-through'))) {
                ele.querySelector('.activity').classList.toggle('strike-through');
            }
        })
    }
   
        
    

    taskObject['notCompleted'].forEach((ele)=>{
        if (!(ele.querySelector('.activity').classList.contains('strike-through'))) {
            activeCount+=1;
        }
    })
    statusOfTasks.innerText = `${activeCount} tasks are remaining`
    if (activeCount == 0) {
        clearBtn.style.display = 'inline-block';
    } else {
        clearBtn.style.display = 'none';
    }
    
    
})

// tasks.addEventListener('dbclick',(event)=>{
//     if (event.target.classList.contains('.activity')) {

//     }
// })








